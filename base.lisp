(in-package :%cl-math)

;; (defvar *!equivs* (make-hash-table))
;; (defun setter (type) (car (gethash type *!equivs*)))
;; (defun getter (type) (cdr (gethash type *!equivs*)))
;; (defmacro deftype-1 (type setter getter)
;;   `(progn
;;      (deftype name type)
;;      (setf (gethash type *!equivs*) (cons setter getter))))

(defmacro defmathfn
    (name args (&optional &key (inline t) docstring) &body body) 
  "A defun facility standardised for this library, meant for mutation of objects.
 * non-destructive (consing/mallocing) (~-postfixed) and 
 * destructive (non-consing) versions (as per NAME)
 * inline (suitable for most math functions)" 
  (let ((!-name (intern (concatenate 'string (symbol-name name) "~"))))
    `(progn
       ,@(list
          (if inline
              `(declaim (inline ,!-name)))
          `(defun ,name ,args
             ,@(remove nil
                       `(,(if docstring (concatenate 'string docstring "
Results are stored in the first argument."))
                         ,@body)))
          (if inline
              `(declaim (inline ,name))) 
          `(defun ,!-name ,args
             ,@(remove nil
                       `(,(if docstring (concatenate 'string docstring "
Results are stored in a new instance of type."))
                         (,name
                          ,@(let ((actual-args (remove-if (lambda (x) (equalp #\& (string x))) args)))
                              `((,(find-symbol "COPY" *package*) ,(car actual-args)) ,@(cdr actual-args)))))))))))

(defmacro defn
    (name lambda-list &body body) 
  "A defun facility standardised for this library, meant for calculating values.
 * inline (suitable for most math functions since everything is largely convenience-esque)" 
  `(progn
     (declaim (inline ,name))
     (defun ,name ,lambda-list
       ;; (parse-body body)
       ;; ,@body
       ,@(multiple-value-bind (body decls doc)
             (parse-body body :documentation t)
           (let* ((decls (reduce #'append (mapcar #'rest decls))))
             (append
              (if doc `(,doc))
              ;; `((declare
              ;;    (optimize (speed 3) (safety 1) (debug 1)))
              ;;   (declare ,@decls))
              `((declare ,@decls)) 
              body))))))

(in-package :cl-math)

(defun floatify! (vec)
  (loop for i across vec
        do (setf (aref vec i)
                 (float (aref vec i)))))

(defun weighted-random (els weights)
  (declare (dynamic-extent els weights))
  (let* ((sum (loop for w in weights sum w))
         (weight (random (coerce sum 'float))))
    (loop for e in els
          for w in weights
          sum w into current-weight
          while (< current-weight weight)
          finally (return e))))
