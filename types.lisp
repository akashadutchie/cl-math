(in-package :cl-math)
(deftype vec2 () '(simple-array single-float (2)))
(deftype vec4 () '(simple-array single-float (4))) 
