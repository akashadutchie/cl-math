(uiop:define-package #:%cl-math
  (:use #:cl #:alexandria)
  (:shadow :== :++ :-- :** :// :length) 
  (:export #:defn #:defmathfn)) 

(uiop:define-package #:cl-math
  (:nicknames #:math)
  (:use #:cl #:alexandria #:%cl-math) 
  (:shadow :++ :-- :** :// :length :lerp)
  (:export
   #:floatify!
   #:range-scale 
   #:τ
   #:π
   #:+one-degree-in-radians+
   #:+one-radian-in-degrees+
   #:diff
   #:dist
   #:weighted-random
   #:explode 
   #:implode
   #:lerpf
   #:line-seg-intersection-p
   #:vec2 
   #:vec4)
  ;; (:reexport :cl-math.vec2)
  )

(uiop:define-package #:cl-math.vec2
  (:nicknames #:vec2)
  (:use #:cl #:alexandria #:%cl-math #:cl-math) 
  (:shadow :== :++ :-- :** :// :length :lerp)
  (:export
   #:new
   #:zero 
   #:*zero*
   #:up
   #:down
   #:left 
   #:right
   #:x
   #:y
   #:copy
   #:vec2
   #:++
   #:--
   #:**
   #://
   #:len2
   #:len
   #:dst
   #:dot
   #:cross
   #:determinant
   #:approach
   #:angle
   #:angle-to 
   #:lerp
   #:lerp2
   #:normalise
   #:scl
   #:absv
   #:vector2-intersect-p
   ))

(uiop:define-package #:cl-math.vec4
  (:nicknames #:vec4)
  (:use #:cl #:alexandria #:%cl-math #:cl-math) 
  (:shadow :== :++ :-- :** :// :length :lerp)
  (:export
   #:new
   #:zero
   #:up
   #:down
   #:left 
   #:right
   #:x
   #:y 
   #:z
   #:w
   #:copy
   #:vec4
   #:++
   #:--
   #:**
   #://
   #:len2
   #:len
   #:dst
   #:dot
   #:cross
   #:determinant
   #:approach
   #:angle
   #:angle-to 
   #:lerp
   #:lerp2
   #:normalise
   #:scl
   #:absv
   #:vector4-intersect-p
   ))
