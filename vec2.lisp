(in-package :cl-math.vec2)

;; (defconstant up    #(0 1))
;; (defconstant down  #(0 -1))
;; (defconstant left  #(-1 0))
;; (defconstant right #(1 0))

;; All functions that return a data structure are destructive.

;; Accessors and setteres

(defn new (&optional (x 0f0) (y 0f0))
    (let ((res (make-array '(2) :element-type 'single-float)))
      (setf (aref res 0) (coerce x 'single-float))
      (setf (aref res 1) (coerce y 'single-float))
      (the math:vec2 res)))

(defn x (vec2)
    (declare (type vec2 vec2))
  (aref vec2 0))

(defn y (vec2)
    (declare (type vec2 vec2))
  (aref vec2 1))

(defn (setf x) (val vec2)
    (declare (type vec2 vec2))
  (setf (aref vec2 0) (coerce val 'single-float)))

(defn (setf y) (val vec2)
    (declare (type vec2 vec2))
  (setf (aref vec2 1) (coerce val 'single-float)))

(defn copy (vec) (copy-seq vec))

(defn (setf vec2) (target source)
    "Recommended to use vector literals for the source vector #(x y)
or an already instantiated vec2"
  (setf (aref source 0) (aref target 0)
        (aref source 1) (aref target 1)))

;; Misc
(defun zero (&rest vecs)
  (declare (dynamic-extent vecs))
  (setf (x (car vecs)) 0f0
        (y (car vecs)) 0f0)
  (unless (endp (cdr vecs))
    (zero (cdr vecs))))

(defvar %*zero* (new))
(define-symbol-macro *zero* (progn (zero %*zero*) %*zero*))


;; Utility functions

(defn -- (vec2-a &rest vec2s)
    (declare (dynamic-extent vec2s)) 
  (map nil (lambda (v)
             (decf (x vec2-a) (x v))
             (decf (y vec2-a) (y v)))
       vec2s)
  vec2-a)

(defn ++ (vec2-a &rest vec2s) 
    (map nil (lambda (v)
               (incf (x vec2-a) (x v))
               (incf (y vec2-a) (y v)))
         vec2s)
  vec2-a)

(defn ** (vec2-a &rest vec2s)
    (map nil (lambda (v)
               (setf (x vec2-a) (* (x vec2-a) (x v)))
               (setf (y vec2-a) (* (y vec2-a) (y v))))
         vec2s)
  vec2-a)

(defn // (vec2-a &rest vec2s)
    (map nil (lambda (v)
               (setf (x vec2-a) (/ (x vec2-a) (x v)))
               (setf (y vec2-a) (/ (y vec2-a) (y v))))
         vec2s)
  vec2-a)

;;

(defn len2 (vec2)
    (+ (* (x vec2) (x vec2)) (* (y vec2) (y vec2))))

(defn len (vec2)
    (sqrt (len2 vec2)))

(defn dst (vec2-a vec2-b)
    ;;(declare (type vec2 a b))
    "Calculates the distance between two 2-dimensional vectors" 
  (dist (x vec2-a) (y vec2-a) (x vec2-b) (y vec2-b)))

(defn dot (vec2-a vec2-b)
    ;;(declare (type vec2 vec2-a vec2-b))
    ;; x1*x2 + y1*y2
    (+ (* (x vec2-a) (x vec2-b)) (* (y vec2-a) (y vec2-b))))

(defn cross (vec2-a vec2-b)
    ;;(declare (type vec2 vec2-a vec2-b))
    ;; x1*x2 + y1*y2
    (- (* (x vec2-a) (y vec2-b)) (* (y vec2-a) (x vec2-b))))

(defn determinant (vec2-a vec2-b)
    ;;(declare (type vec2 vec2-a vec2-b))
    ;; x1*y2 - y1*x2
    (- (* (x vec2-a) (y vec2-b)) (* (y vec2-a) (x vec2-b))))

(defmathfn approach (vec2-a vec2-b amount) 
    (:docstring "Moves vec2-a towards vec2-b by amount" :inline nil)
  ;;(declare (type vec2 vec2-a vec2-b))
  (let ((ox (x vec2-a))
        (oy (y vec2-b)))
    (-- vec2-b vec2-a)
    (normalise vec2-a)
    (setf (x vec2-a) (+ ox (* amount (x vec2-a)))
          (y vec2-a) (+ oy (* amount (y vec2-a))))
    vec2-a))

(defn angle (vec2)
    ;;(declare (type vec2 vec2))
    (atan (aref vec2 1) (aref vec2 0)))

(defn angle-to (vec2-a vec2-b)
    ;;(declare (type vec2 vec2-a vec2-b))
    ;; (atan (determinant vec2-a vec2-b) (dot vec2-a vec2-b))
    ;; (atan (cross vec2-a vec2-b) (dot vec2-a vec2-b))
    (let ((dx (- (x vec2-b) (x vec2-a)))
          (dy (- (y vec2-b) (y vec2-a))))
      (atan dy dx))
  )

(defmathfn lerp (vec2-a vec2-b percent)
    (:docstring
     "Linearly interpolates vec2-a towards vec2-b" :inline nil)
  ;;(declare (type vec2 vec2-a vec2-b))
  (setf
   (x vec2-a) (lerpf (x vec2-a) (x vec2-b) percent)
   (y vec2-a) (lerpf (y vec2-a) (y vec2-b) percent))
  vec2-a)

(defmathfn lerp2 (vec2-res vec2-a vec2-b percent)
    (:docstring
     "Linearly interpolates vec2-a towards vec2-b and stores the result in vec2-res"
     :inline nil)
  ;;(declare (type vec2 vec2-a vec2-b))
  (setf
   (x vec2-res) (lerpf (x vec2-a) (x vec2-b) percent)
   (y vec2-res) (lerpf (y vec2-a) (y vec2-b) percent))
  vec2-res)

(defmathfn normalise (vec2-a)
    (:docstring
     "Normalises vec2")
  ;;(declare (type vec2 vec2))
  (let ((len (len vec2-a)))
    (unless (eq len 0f0)
      (setf
       (x vec2-a) (/ (x vec2-a) len)
       (y vec2-a) (/ (y vec2-a) len)))
    vec2-a))

(defmathfn scl (vec2 scalar)
    (:docstring
     "Multiplies vector by a scalar") 
  (setf
   (aref vec2 0) (* (aref vec2 0) scalar)
   (aref vec2 1) (* (aref vec2 1) scalar))
  vec2)

(defmathfn absv (vec2)
    (:docstring
     "Sets the components of a vector to their absolutes") 
  (setf
   (aref vec2 0) (abs (aref vec2 0))
   (aref vec2 1) (abs (aref vec2 1)))
  vec2)

;; TODO more math functions needed.

(defn vector2-intersect-p (vec2-a vec2-b vec2-c vec2-d)
    ;;(declare (type vec2 vec2-a vec2-b vec2-c vec2-d))
    "Returns t if the lines formed by the vectors would intersect, nil otherwise.
The line segments are described as (l vec2-a vec2-b) (l vec2-c vec2-d)"
  (line-seg-intersection-p
   (x vec2-a)
   (y vec2-a)
   (x vec2-b)
   (y vec2-b) 
   (x vec2-c)
   (y vec2-c) 
   (x vec2-d)
   (y vec2-d)))

