(asdf:defsystem #:cl-math
  :version "0.1"
  :license "MIT"
  :serial t 
  :depends-on (#:alexandria)
  :components ((:file "package")
               (:file "types")
               (:file "base")
               (:file "math")
               (:file "vec2")
               (:file "vec4")))

;; TODO declare types in different packages

