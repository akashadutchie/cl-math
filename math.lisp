(in-package :cl-math)

(defconstant τ 6.283185307179586d0) 
(defconstant π 3.141592653589793f0)
(defconstant +one-degree-in-radians+ 0.017453292519943295f0)
(defconstant +one-radian-in-degrees+ 57.29577951308232f0)

(defn range-scale (val in-min in-max out-min out-max)
    "Translates a value from one range to another. That is:
   - a value of in-min would get mapped to out-min
   - a value of in-max to out-max
   - values in-between to values in-between
   - etc.

  Does not constrain values to within the range.

  source: https://www.arduino.cc/reference/en/language/functions/math/map/"
  (+ (/ (* (- val in-min) (- out-max out-min)) (- in-max in-min)) out-min))

(defn diff (a b)
    "Difference between two numbers"
  (if (< a b)
      (- b a)
      (- a b)))

(defn dist (x1 y1 x2 y2)
    (declare (optimize (speed 3) (safety 1) (debug 1)))
  (let ((dx (- x2 x1))
        (dy (- y2 y1)))
    (sqrt (+ (* dx dx) (* dy dy)))))

(defn explode (real) 
    "Rounds a real to an integer away from 0"
  (if (< real 0)
      (floor real)
      (ceiling real)))

(defn implode (real)
    "Rounds a real to an integer towards 0"
  (if (< real 0)
      (ceiling real)
      (floor real)))

(defn lerpf (a b percent)
    "Linear interpolation between a and b floats"
  (declare (single-float a b percent))
  ;; Thanks SO and google for alerting me about different lerp algorithms
  ;; who knew it could be so complicated?
  ;; https://stackoverflow.com/a/58648036/9107122
  ;; if (t <= 0.5)
  ;; return a+(b-a)*t;
  ;; else
  ;; return b-(b-a)*(1.0-t);
  (if (< percent 0.5f0)
      (+ a (* percent (- b a)))
      (- b (* (- 1f0 percent) (- b a)))))

;; source http://www.stefanbader.ch/?p=49 (wayback-machine it)
(defun line-seg-intersection-p (ax1 ay1 ax2 ay2 bx1 by1 bx2 by2)
  "Returns t if the lines formed by the coordinates described would intersect, nil otherwise.
The line segments are described as (l (p ax1 ay1) (p ax2 ay2)) (l (p bx1 by1) (p bx2 by2))" 
  (let ((ax (- ax2 ax1))
        (ay (- ay2 ay1))
        (bx (- bx1 bx2))
        (by (- by1 by2))
        (cx (- ax1 bx1))
        (cy (- ay1 by1)))
    (let ((an (- (* by cx) (* bx cy)))
          (ad (- (* ay bx) (* ax by)))
          (bn (- (* ax cy) (* ay cx)))
          (bd (- (* ay bx) (* ax by))))
      (unless (or (eq 0 ad) (eq 0 bd))          
        (let ((res t))
          (if (< 0 ad)
              (setq res (not (or (< an 0) (< ad an))))
              (setq res (not (or (< 0 an) (< an ad)))))
          (if (and res (< 0 bd))
              (setq res (not (or (< bd 0) (< bd bn))))
              (setq res (not (or (< 0 bd) (< bn bd)))))
          res)))))

;; static bool FasterLineSegmentIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {

;; Vector2 a = p2 - p1;
;; Vector2 b = p3 - p4;
;; Vector2 c = p1 - p3;

;; float alphaNumerator = b.y*c.x - b.x*c.y;
;; float alphaDenominator = a.y*b.x - a.x*b.y;
;; float betaNumerator  = a.x*c.y - a.y*c.x;
;; float betaDenominator  = a.y*b.x - a.x*b.y;

;; bool doIntersect = true;

;; if (alphaDenominator == 0 || betaDenominator == 0) {
;; doIntersect = false;
;; } else {

;; if (alphaDenominator > 0) {
;; if (alphaNumerator < 0 || alphaNumerator > alphaDenominator) {
;; doIntersect = false;

;; }
;; } else if (alphaNumerator > 0 || alphaNumerator < alphaDenominator) {
;; doIntersect = false;
;; }

;; if (doIntersect  betaDenominator > 0) {
;; if (betaNumerator < 0 || betaNumerator > betaDenominator) {
;; doIntersect = false;
;; }
;; } else if (betaNumerator > 0 || betaNumerator < betaDenominator) {
;; doIntersect = false;
;; }
;; }

;; return doIntersect;
;; }
